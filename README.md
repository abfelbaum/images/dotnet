# dotnet images

This project provides docker images for dotnet.

Information can be found in the docs/ folder or on https://rwthapp.pages.rwth-aachen.de/misc/container-images/dotnet/
